load '/opt/bats-support/load.bash'
load '/opt/bats-assert/load.bash'

@test "Prints usage and exits with error when invalid option provided." {
  source ./templar.sh

  run templar_main --fail-it

  assert_failure
  assert_output --partial "Usage:"
}

@test "Prints usage and exits with success when -h or --help." {
  source ./templar.sh

  run templar_main -h

  assert_success
  assert_output --partial "Usage:"

  run templar_main --help

  assert_success
  assert_output --partial "Usage:"
}

@test "Reads from stdin when no files provided." {
  source ./templar.sh

  run templar_main < <(cat ./test/tpl/empty.tpl)

  assert_success
  assert_output ''
}

@test "Reads file." {
  source ./templar.sh

  run templar_main ./test/tpl/empty.tpl

  assert_success
  assert_output ''
}

@test "Reads multiple files." {
  source ./templar.sh

  run templar_main ./test/tpl/empty.tpl ./test/tpl/empty.tpl

  assert_success
  assert_output ''
}

@test "Handles unbound template variable." {
  source ./templar.sh

  run templar_main ./test/tpl/simple.tpl

  assert_success
  assert_output ''
}

@test "Handles template variable containing escaped & unescaped quotes (declared in double quotes)." {
  source ./templar.sh

  declare -A inputs=(
    [single]="'"
    [single1]="\'"
    [single2]="\\'"
    [single3]="\\\'"
    [double]="invalid"
    [double1]="\""
    [double2]="invalid"
    [double3]="\\\""
    [double4]="invalid"
    [double5]="\\\\\""
    [backtick]="invalid"
    [backtick1]="\`"
    [backtick2]="invalid"
    [backtick3]="\\\`"
    [backtick4]="invalid"
    [backtick5]="\\\\\`"
  )

  declare -A outputs=(
    [single]="'"
    [single1]="\'"
    [single2]="\'"
    [single3]="\\\'"
    [double]="invalid"
    [double1]='"'
    [double2]="invalid"
    [double3]='\"'
    [double4]="invalid"
    [double5]='\\"'
    [backtick]="invalid"
    [backtick1]='`'
    [backtick2]="invalid"
    [backtick3]='\`'
    [backtick4]="invalid"
    [backtick5]='\\`'
  )

  declare -A messages=(
    [single]="\"'\"      -> '"
    [single1]="\"\'\"     -> \'"
    [single2]="\"\\\'\"    -> \'"
    [single3]="\"\\\\\'\"   -> \\\'"
    [double]="\"\"\"      -> invalid"
    [double1]="\"\\\"\"     -> \""
    [double2]="\"\\\\\"\"    -> invalid"
    [double3]="\"\\\\\\\"\"   -> \\\""
    [double4]="\"\\\\\\\\\"\"  -> invalid"
    [double5]="\"\\\\\\\\\\\"\" -> \\\\\""
    [backtick]="\"\`\"      -> invalid"
    [backtick1]="\"\\\`\"     -> \`"
    [backtick2]="\"\\\\\`\"    -> invalid"
    [backtick3]="\"\\\\\\\`\"   -> \\\`"
    [backtick4]="\"\\\\\\\\\`\"  -> invalid"
    [backtick5]="\"\\\\\\\\\\\`\" -> \\\\\`"
  )

  for key in "${!inputs[@]}"; do
    printf '# %s\n' "${messages[$key]}" >&3

    export TPL_VAR=${inputs[$key]}
    run templar_main ./test/tpl/simple.tpl

    assert_success
    assert_output "${outputs[$key]}"
  done
}

@test "Handles template variable containing escaped & unescaped quotes (locale translation)." {
  source ./templar.sh

  declare -A inputs=(
    [single]=$"'"
    [single1]=$"\'"
    [single2]=$"\\'"
    [single3]=$"\\\'"
    [double]="invalid"
    [double1]=$"\""
    [double2]="invalid"
    [double3]=$"\\\""
    [double4]="invalid"
    [double5]=$"\\\\\""
    [backtick]="invalid"
    [backtick1]=$"\`"
    [backtick2]="invalid"
    [backtick3]=$"\\\`"
    [backtick4]="invalid"
    [backtick5]=$"\\\\\`"
  )

  declare -A outputs=(
    [single]="'"
    [single1]="\'"
    [single2]="\'"
    [single3]="\\\'"
    [double]="invalid"
    [double1]='"'
    [double2]="invalid"
    [double3]='\"'
    [double4]="invalid"
    [double5]='\\"'
    [backtick]="invalid"
    [backtick1]='`'
    [backtick2]="invalid"
    [backtick3]='\`'
    [backtick4]="invalid"
    [backtick5]='\\`'
  )

  declare -A messages=(
    [single]="$\"'\"      -> '"
    [single1]="$\"\'\"     -> \'"
    [single2]="$\"\\\'\"    -> \'"
    [single3]="$\"\\\\\'\"   -> \\\'"
    [double]="$\"\"\"      -> invalid"
    [double1]="$\"\\\"\"     -> \""
    [double2]="$\"\\\\\"\"    -> invalid"
    [double3]="$\"\\\\\\\"\"   -> \\\""
    [double4]="$\"\\\\\\\\\"\"  -> invalid"
    [double5]="$\"\\\\\\\\\\\"\" -> \\\\\""
    [backtick]="$\"\`\"      -> invalid"
    [backtick1]="$\"\\\`\"     -> \`"
    [backtick2]="$\"\\\\\`\"    -> invalid"
    [backtick3]="$\"\\\\\\\`\"   -> \\\`"
    [backtick4]="$\"\\\\\\\\\`\"  -> invalid"
    [backtick5]="$\"\\\\\\\\\\\`\" -> \\\\\`"
  )

  for key in "${!inputs[@]}"; do
    printf '# %s\n' "${messages[$key]}" >&3

    export TPL_VAR=${inputs[$key]}
    run templar_main ./test/tpl/simple.tpl

    assert_success
    assert_output "${outputs[$key]}"
  done
}

@test "Handles template variable containing escaped & unescaped quotes (declared in single quotes)." {
  source ./templar.sh

  declare -A inputs=(
    [single]="invalid"
    [single1]="invalid"
    [single2]="invalid"
    [double]='"'
    [double1]='\"'
    [double2]='\\"'
    [backtick]='`'
    [backtick1]='\`'
    [backtick2]='\\`'
  )

  declare -A outputs=(
    [single]="invalid"
    [single1]="invalid"
    [single2]="invalid"
    [double]='"'
    [double1]='\"'
    [double2]='\\"'
    [backtick]='`'
    [backtick1]='\`'
    [backtick2]='\\`'
  )

  declare -A messages=(
    [single]="'''   -> invalid"
    [single1]="'\''  -> invalid"
    [single2]="'\\\\'' -> invalid"
    [double]="'\"'   -> \""
    [double1]="'\\\"'  -> \\\""
    [double2]="'\\\\\"' -> \\\\\""
    [backtick]="'\`'   -> \`"
    [backtick1]="'\\\`'  -> \\\`"
    [backtick2]="'\\\\\`' -> \\\\\`"
  )

  for key in "${!inputs[@]}"; do
    printf '# %s\n' "${messages[$key]}" >&3

    export TPL_VAR=${inputs[$key]}
    run templar_main ./test/tpl/simple.tpl

    assert_success
    assert_output "${outputs[$key]}"
  done
}

@test "Handles template variable containing escaped & unescaped quotes (ANSI C)." {
  source ./templar.sh

  declare -A inputs=(
    [single]="invalid"
    [single1]=$'\''
    [single2]="invalid"
    [single3]=$'\\\''
    [single4]="invalid"
    [single5]=$'\\\\\''
    [double]=$'"'
    [double1]=$'\"'
    [double2]=$'\\"'
    [double3]=$'\\\"'
    [double4]=$'\\\\"'
    [backtick]=$'`'
    [backtick1]=$'\`'
    [backtick2]=$'\\`'
    [backtick3]=$'\\\`'
  )

  declare -A outputs=(
    [single]="invalid"
    [single1]="'"
    [single2]="invalid"
    [single3]="\'"
    [single4]="invalid"
    [single5]="\\\'"
    [double]='"'
    [double1]='"'
    [double2]='\"'
    [double3]='\"'
    [double4]='\\"'
    [backtick]='`'
    [backtick1]='\`'
    [backtick2]='\`'
    [backtick3]='\\`'
  )

  declare -A messages=(
    [single]="$'''      -> invalid"
    [single1]="$'\''     -> '"
    [single2]="$'\\\''    -> invalid"
    [single3]="$'\\\\\''   -> \'"
    [single4]="$'\\\\\\\''  -> invalid"
    [single5]="$'\\\\\\\\\'' -> \\\'"
    [double]="$'\"'      -> \""
    [double1]="$'\\\"'     -> \""
    [double2]="$'\\\\\"'    -> \\\""
    [double3]="$'\\\\\\\"'   -> \\\""
    [double4]="$'\\\\\\\\\"'  -> \\\\\""
    [backtick]="$'\`'      -> \`"
    [backtick1]="$'\\\`'     -> \\\`"
    [backtick2]="$'\\\\\`'    -> \\\`"
    [backtick3]="$'\\\\\\\`'   -> \\\\\`"
  )

  for key in "${!inputs[@]}"; do
    printf '# %s\n' "${messages[$key]}" >&3

    export TPL_VAR=${inputs[$key]}
    run templar_main ./test/tpl/simple.tpl

    assert_success
    assert_output "${outputs[$key]}"
  done
}

@test "Handles template variable containing non-whitespace characters." {
  source ./templar.sh

  declare -a inputs=(
    quickbrownfoxjumpsoverthelazydog
    QUICKBROWNFOXJUMPSOVERTHELAZYDOG
    '~1234567890!@#$%^&*()-_=+[{]}\|;:,<.>/?'
  )

  declare -a outputs=(
    quickbrownfoxjumpsoverthelazydog
    QUICKBROWNFOXJUMPSOVERTHELAZYDOG
    '~1234567890!@#$%^&*()-_=+[{]}\|;:,<.>/?'
  )

  declare -a messages=(
    quickbrownfoxjumpsoverthelazydog
    QUICKBROWNFOXJUMPSOVERTHELAZYDOG
    "'~1234567890!@#$%^&*()-_=+[{]}\|;:,<.>/?'"
  )

  for key in "${!inputs[@]}"; do
    printf '# %s\n' "${messages[$key]}" >&3

    export TPL_VAR=${inputs[$key]}
    run templar_main ./test/tpl/simple.tpl

    assert_success
    assert_output "${outputs[$key]}"
  done
}

@test "Handles template variable containing whitespace characters." {
  source ./templar.sh

  declare -a inputs=(
    'Hello World!'
    $'Hello\nWorld!'
    $'Hello\rWorld!'
    $'Hello\tWorld!'
    $'Hello\vWorld!'
  )

  declare -a outputs=(
    'Hello World!'
    $'Hello\nWorld!'
    $'Hello\rWorld!'
    $'Hello\tWorld!'
    $'Hello\vWorld!'
  )

  declare -a messages=(
    "'Hello World!'"
    "$'Hello\nWorld!'"
    "$'Hello\rWorld!'"
    "$'Hello\tWorld!'"
    "$'Hello\vWorld!'"
  )

  for key in "${!inputs[@]}"; do
    printf '# %s\n' "${messages[$key]}" >&3

    export TPL_VAR=${inputs[$key]}
    run templar_main ./test/tpl/simple.tpl

    assert_success
    assert_output "${outputs[$key]}"
  done
}

@test "Handles incomplete template variable declarations." {
  source ./templar.sh

  export TPL_VAR=test
  run templar_main ./test/tpl/incomplete.tpl

  assert_success
  assert_output "TPL_VAR
{TPL_VAR
TPL_VAR}
{TPL_VAR}
{{TPL_VAR
TPL_VAR}}
{{TPL_VAR}
{TPL_VAR}}
$TPL_VAR
{$TPL_VAR
$TPL_VAR}
{$TPL_VAR}
{{$TPL_VAR}
{$TPL_VAR}}
{{$TPL_VAR}}"
}

@test "Handles multiple files." {
  source ./templar.sh

  export TPL_VAR=test
  run templar_main ./test/tpl/simple.tpl ./test/tpl/simple.tpl

  assert_success
  assert_output $'test\ntest'
}


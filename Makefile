################################################################################
# Parameters
################################################################################

TEST_PATH?=test/templar.bats

################################################################################
# Rules
################################################################################

.PHONY: test
test: templar.sh
	rm -rf "$(CURDIR)/coverage"
	docker run -v "$(CURDIR)":/mnt/workspace --rm --name cave registry.gitlab.com/agogpixel/devops/docker/cave:latest $(TEST_PATH)

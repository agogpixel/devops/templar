# templar

Replace strings of form `{{VAR}}` with corresponding `$VAR` found in environment and prints results to `stdout`. When no files specified, reads from stdin.

## Usage

```bash
templar [<template>]...
```

When no files specified, reads from stdin.

## Development

- Project utilizes [GitHub flow](https://guides.github.com/introduction/flow/).

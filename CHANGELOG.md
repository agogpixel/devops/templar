# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Latest]

### Added

- Support multiple files.

## [7cbc8209]

### Added

- Minimalist implementation.

[Latest]: https://gitlab.com/agogpixel/devops/templar/-/compare/7cbc8209...master
[7cbc8209]: https://gitlab.com/agogpixel/devops/templar/-/tree/7cbc8209

#!/bin/bash

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset
#set -o xtrace

################################################################################
# Runtime Settings
################################################################################

# User provided template files. Default to stdin.
TEMPLAR_TPL_SOURCES=-

################################################################################
# Options
################################################################################

# Print help.
declare -A TEMPLAR_OPT_HELP=([NAME]=help [LONG]=help [SHORT]=h)

################################################################################
# Functions
################################################################################

templar_main() {
  templar_parse_args $@
  templar_transform_stream < <(cat -- $TEMPLAR_TPL_SOURCES)
}

templar_parse_args() {
  local opts="${TEMPLAR_OPT_HELP[SHORT]}"
  local longopts="${TEMPLAR_OPT_HELP[LONG]}"

  ! PARSED=$(getopt --options=$opts --longoptions=$longopts --name "$0" -- "$@")

  if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has complained about wrong arguments to stdout.
    templar_print_usage
    exit 1
  fi

  # Handle quoting in getopt output.
  eval set -- "$PARSED"

  while true; do
    case "$1" in
      "-${TEMPLAR_OPT_HELP[SHORT]}"|"--${TEMPLAR_OPT_HELP[LONG]}")
        printf '\nReplace strings of form {{VAR}} with corresponding $VAR found in environment\n'
        printf 'and prints results to stdout.\n\n'
        templar_print_usage
        printf '\nSee https://gitlab.com/agogpixel/devops/templar for more info.\n'
        exit 0
        ;;
      --)
        shift
        break
        ;;
    esac
  done

  if [ "$#" -gt 0 ]; then
    # File(s).
    TEMPLAR_TPL_SOURCES="$@"
  fi
}

templar_print_usage() {
  cat <<EOF
Usage:
  $0 [<template>]...

  When no files specified, reads from stdin.
EOF
}

templar_transform_stream() {
  local full_match_regex='^\{{2}([A-Z_][A-Z0-9_]+)\}{2}$'
  local candidate_ok_regex='^(\{{2}[A-Z_][A-Z0-9_]+\}|\{{2}[A-Z_][A-Z0-9_]+|\{{2}[A-Z_]|\{{2})$'

  local buffer=''
  local candidate=''

  # Because escaping backslashes & quotes with sed is not great...
  while IFS= read -d '' -rN1 ch; do
    if [ -z "$candidate" ]; then
      if [ "$ch" = '{' ]; then
        candidate="$ch"
      else
        printf '%s' "$ch"
      fi

      continue
    fi

    buffer="$candidate$ch"

    if [[ "$buffer" =~ $full_match_regex ]]; then
      # Matched, replace.
      candidate=''
      templar_eval_value_as_variable "${BASH_REMATCH[1]}"
    elif [[ "$buffer" =~ $candidate_ok_regex ]]; then
      # Continue candidate.
      candidate="$buffer"
    elif [ "$buffer" = '{{{' ]; then
      # Shift candidate.
      candidate='{{'
      printf '{'
    else
      # Candidate does not resolve.
      candidate=''
      printf '%s' "$buffer"
    fi
  done
}

templar_eval_value_as_variable() {
  set +o nounset
  eval printf '%s' \"\$$1\"
  set -o nounset
}

################################################################################
# Program Entry
################################################################################

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  templar_main $@
fi
